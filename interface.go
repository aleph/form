package form

import (
	"math/big"

	"gitlab.com/aleph-project/form/value"
)

// A Head is the head of a form
type Head interface {
	// Kind returns the kind of the head
	Kind() HeadKind
	// Name returns the name of the head
	Name() string
}

// A SymbolicHead is a head that does not have a specific kind
type SymbolicHead interface {
	Head
	// ID returns the ID of a known head, or zero
	ID() uint64
}

// A FormHead is a form that is used as a head
type FormHead interface { // nolint: golint
	Head
	// Form returns the form head's form
	Form() Form
}

// Form is a form, which has a head and a number of elements
type Form interface {
	// Head returns the head of the form
	Head() Head
	// Depth returns the depth, or number of elements, of the form
	Depth() uint32
	// GetElement returns an indexed element of the form
	GetElement(index uint32) (Form, bool)
	// GetElements returns an array of all of the elements of the form
	GetElements() []Form
}

// PrimitiveForm is a form that has a value but zero elements
type PrimitiveForm interface {
	Form
	// Value returns the value of the primitive form
	Value() interface{}
}

// TensorForm is the form of a set of values that represents a tensor
type TensorForm interface {
	Form
	// Dimensionality returns the number of dimensions of the tensor
	Dimensionality() int
	// GetDimension returns the value of the index dimension
	GetDimension(index int) (uint32, bool)
	// GetDimensions returns an array of the dimensions of the tensor
	GetDimensions() []uint32
	// GetValue returns the indexed value of the tensor
	GetValue(indices ...uint32) (Form, bool)
}

// NumericForm is the form of a numeric primitive
type NumericForm interface {
	PrimitiveForm
	// IsLarge returns whether the numeric value is larger than machine-precision
	IsLarge() bool
	IsInteger() bool
	IsRational() bool
	IsReal() bool
	SmallIntegerValue() int64
	LargeIntegerValue() *big.Int
	SmallRationalValue() value.Rational
	LargeRationalValue() *big.Rat
	SmallRealValue() float64
	LargeRealValue() *big.Float
}

// RationalForm is the form of a rational primitive
type RationalForm interface {
	NumericForm
	NumeratorValue() NumericForm
	DenominatorValue() NumericForm
}
