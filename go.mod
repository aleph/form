module gitlab.com/aleph-project/form

require (
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/colour v0.0.0-20160524082231-60882d9e2721 // indirect
	github.com/alecthomas/repr v0.0.0-20180920225502-7ed41413b477 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/sys v0.0.0-20181011152604-fa43e7bc11ba // indirect
)
