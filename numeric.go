// nolint: golint
package form

import (
	"fmt"
	"math/big"

	"gitlab.com/aleph-project/form/value"
)

func shrinkRat(r *big.Rat) value.Rational {
	return rational(
		r.Num().Int64(),
		r.Denom().Int64(),
	)
}

func ratToInt(r *big.Rat) *big.Int {
	return new(big.Int).Div(r.Num(), r.Denom())
}

// verify that each type does conform to the numeric form interface
var (
	_ NumericForm  = new(SmallIntegerForm)
	_ RationalForm = new(SmallRationalForm)
	_ NumericForm  = new(SmallRealForm)
	_ NumericForm  = new(LargeIntegerForm)
	_ RationalForm = new(LargeRationalForm)
	_ NumericForm  = new(LargeRealForm)
)

// SmallIntegerForm is a machine-precision integer primitive form
type SmallIntegerForm int64

func (f SmallIntegerForm) Raw() int64                         { return int64(f) }
func (f SmallIntegerForm) String() string                     { return fmt.Sprint(f.Raw()) }
func (f SmallIntegerForm) Head() Head                         { return IntegerHead }
func (f SmallIntegerForm) Depth() uint32                      { return 0 }
func (f SmallIntegerForm) GetElement(uint32) (Form, bool)     { return nil, false }
func (f SmallIntegerForm) GetElements() []Form                { return noElements }
func (f SmallIntegerForm) Value() interface{}                 { return f.Raw() }
func (f SmallIntegerForm) IsLarge() bool                      { return false }
func (f SmallIntegerForm) IsInteger() bool                    { return true }
func (f SmallIntegerForm) IsRational() bool                   { return false }
func (f SmallIntegerForm) IsReal() bool                       { return false }
func (f SmallIntegerForm) SmallIntegerValue() int64           { return f.Raw() }
func (f SmallIntegerForm) LargeIntegerValue() *big.Int        { return big.NewInt(f.Raw()) }
func (f SmallIntegerForm) SmallRationalValue() value.Rational { return rational(f.Raw(), 1) }
func (f SmallIntegerForm) LargeRationalValue() *big.Rat       { return big.NewRat(f.Raw(), 1) }
func (f SmallIntegerForm) SmallRealValue() float64            { return float64(f) }
func (f SmallIntegerForm) LargeRealValue() *big.Float         { return big.NewFloat(float64(f)) }

// SmallRationalForm is a machine-precision rational primitive form
type SmallRationalForm value.Rational

func (f SmallRationalForm) Raw() value.Rational                { return value.Rational(f) }
func (f SmallRationalForm) String() string                     { return fmt.Sprint(f.Raw()) }
func (f SmallRationalForm) Head() Head                         { return RationalHead }
func (f SmallRationalForm) Depth() uint32                      { return 0 }
func (f SmallRationalForm) GetElement(uint32) (Form, bool)     { return nil, false }
func (f SmallRationalForm) GetElements() []Form                { return noElements }
func (f SmallRationalForm) Value() interface{}                 { return f.Raw() }
func (f SmallRationalForm) IsLarge() bool                      { return false }
func (f SmallRationalForm) IsInteger() bool                    { return false }
func (f SmallRationalForm) IsRational() bool                   { return true }
func (f SmallRationalForm) IsReal() bool                       { return false }
func (f SmallRationalForm) SmallIntegerValue() int64           { return f.Raw().Integer() }
func (f SmallRationalForm) LargeIntegerValue() *big.Int        { return big.NewInt(f.Raw().Integer()) }
func (f SmallRationalForm) SmallRationalValue() value.Rational { return f.Raw() }
func (f SmallRationalForm) LargeRationalValue() *big.Rat       { return f.Raw().Large() }
func (f SmallRationalForm) SmallRealValue() float64            { return f.Raw().Real() }
func (f SmallRationalForm) LargeRealValue() *big.Float         { return big.NewFloat(f.Raw().Real()) }
func (f SmallRationalForm) NumeratorValue() NumericForm        { return Integer(f.Numerator) }
func (f SmallRationalForm) DenominatorValue() NumericForm      { return Integer(int64(f.Denominator)) }

// SmallRealForm is a machine-precision real primitive form
type SmallRealForm float64

func (f SmallRealForm) Raw() float64                       { return float64(f) }
func (f SmallRealForm) String() string                     { return fmt.Sprint(f.Raw()) }
func (f SmallRealForm) Head() Head                         { return RealHead }
func (f SmallRealForm) Depth() uint32                      { return 0 }
func (f SmallRealForm) GetElement(uint32) (Form, bool)     { return nil, false }
func (f SmallRealForm) GetElements() []Form                { return noElements }
func (f SmallRealForm) Value() interface{}                 { return f.Raw() }
func (f SmallRealForm) IsLarge() bool                      { return false }
func (f SmallRealForm) IsInteger() bool                    { return false }
func (f SmallRealForm) IsRational() bool                   { return true }
func (f SmallRealForm) IsReal() bool                       { return false }
func (f SmallRealForm) SmallIntegerValue() int64           { return int64(f) }
func (f SmallRealForm) LargeIntegerValue() *big.Int        { return big.NewInt(int64(f)) }
func (f SmallRealForm) SmallRationalValue() value.Rational { panic("not implemented") }
func (f SmallRealForm) LargeRationalValue() *big.Rat       { panic("not implemented") }
func (f SmallRealForm) SmallRealValue() float64            { return f.Raw() }
func (f SmallRealForm) LargeRealValue() *big.Float         { return big.NewFloat(f.Raw()) }

// LargeIntegerForm is an arbitrary precision integer primitive form
type LargeIntegerForm big.Int

func (f *LargeIntegerForm) Raw() *big.Int                      { return (*big.Int)(f) }
func (f *LargeIntegerForm) String() string                     { return fmt.Sprint(f.Raw()) }
func (f *LargeIntegerForm) Head() Head                         { return IntegerHead }
func (f *LargeIntegerForm) Depth() uint32                      { return 0 }
func (f *LargeIntegerForm) GetElement(uint32) (Form, bool)     { return nil, false }
func (f *LargeIntegerForm) GetElements() []Form                { return noElements }
func (f *LargeIntegerForm) Value() interface{}                 { return f.Raw() }
func (f *LargeIntegerForm) IsLarge() bool                      { return true }
func (f *LargeIntegerForm) IsInteger() bool                    { return true }
func (f *LargeIntegerForm) IsRational() bool                   { return false }
func (f *LargeIntegerForm) IsReal() bool                       { return false }
func (f *LargeIntegerForm) SmallIntegerValue() int64           { return f.Raw().Int64() }
func (f *LargeIntegerForm) LargeIntegerValue() *big.Int        { return f.Raw() }
func (f *LargeIntegerForm) SmallRationalValue() value.Rational { return rational(f.Raw().Int64(), 0) }
func (f *LargeIntegerForm) LargeRationalValue() *big.Rat       { return new(big.Rat).SetInt(f.Raw()) }
func (f *LargeIntegerForm) SmallRealValue() float64            { return float64(f.Raw().Int64()) }
func (f *LargeIntegerForm) LargeRealValue() *big.Float         { return new(big.Float).SetInt(f.Raw()) }

// LargeRationalForm is an arbitrary precision rational primitive form
type LargeRationalForm big.Rat

func (f *LargeRationalForm) Raw() *big.Rat                      { return (*big.Rat)(f) }
func (f *LargeRationalForm) String() string                     { return fmt.Sprint(f.Raw()) }
func (f *LargeRationalForm) Head() Head                         { return RationalHead }
func (f *LargeRationalForm) Depth() uint32                      { return 0 }
func (f *LargeRationalForm) GetElement(uint32) (Form, bool)     { return nil, false }
func (f *LargeRationalForm) GetElements() []Form                { return noElements }
func (f *LargeRationalForm) Value() interface{}                 { return f.Raw() }
func (f *LargeRationalForm) IsLarge() bool                      { return true }
func (f *LargeRationalForm) IsInteger() bool                    { return false }
func (f *LargeRationalForm) IsRational() bool                   { return true }
func (f *LargeRationalForm) IsReal() bool                       { return false }
func (f *LargeRationalForm) SmallIntegerValue() int64           { return ratToInt(f.Raw()).Int64() }
func (f *LargeRationalForm) LargeIntegerValue() *big.Int        { return ratToInt(f.Raw()) }
func (f *LargeRationalForm) SmallRationalValue() value.Rational { return shrinkRat(f.Raw()) }
func (f *LargeRationalForm) LargeRationalValue() *big.Rat       { return f.Raw() }
func (f *LargeRationalForm) SmallRealValue() float64            { v, _ := f.Raw().Float64(); return v }
func (f *LargeRationalForm) LargeRealValue() *big.Float         { return new(big.Float).SetRat(f.Raw()) }
func (f *LargeRationalForm) NumeratorValue() NumericForm        { return LargeInteger(f.Raw().Num()) }
func (f *LargeRationalForm) DenominatorValue() NumericForm      { return LargeInteger(f.Raw().Denom()) }

// LargeRealForm is an arbitrary precision real primitive form
type LargeRealForm big.Float

func (f *LargeRealForm) Raw() *big.Float                    { return (*big.Float)(f) }
func (f *LargeRealForm) String() string                     { return fmt.Sprint(f.Raw()) }
func (f *LargeRealForm) Head() Head                         { return RealHead }
func (f *LargeRealForm) Depth() uint32                      { return 0 }
func (f *LargeRealForm) GetElement(uint32) (Form, bool)     { return nil, false }
func (f *LargeRealForm) GetElements() []Form                { return noElements }
func (f *LargeRealForm) Value() interface{}                 { return f.Raw() }
func (f *LargeRealForm) IsLarge() bool                      { return true }
func (f *LargeRealForm) IsInteger() bool                    { return false }
func (f *LargeRealForm) IsRational() bool                   { return true }
func (f *LargeRealForm) IsReal() bool                       { return false }
func (f *LargeRealForm) SmallIntegerValue() int64           { v, _ := f.Raw().Int64(); return v }
func (f *LargeRealForm) LargeIntegerValue() *big.Int        { v, _ := f.Raw().Int(nil); return v }
func (f *LargeRealForm) SmallRationalValue() value.Rational { return shrinkRat(f.LargeRationalValue()) }
func (f *LargeRealForm) LargeRationalValue() *big.Rat       { v, _ := f.Raw().Rat(nil); return v }
func (f *LargeRealForm) SmallRealValue() float64            { v, _ := f.Raw().Float64(); return v }
func (f *LargeRealForm) LargeRealValue() *big.Float         { return f.Raw() }
