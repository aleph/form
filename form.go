package form

import (
	"errors"
	"fmt"
	"strings"
)

// Must will panic if `err` is non-nil, and otherwise returns `form`
func Must(form Form, err error) Form {
	if err != nil {
		panic(err)
	}
	return form
}

// New creates a form with a symbolic head or a known head
func New(head Head, elements ...Form) (Form, error) {
	if KindHasWellDefinedForm(head.Kind()) {
		return nil, errors.New("cannot make a standard form with a head that has a well defined form")
	}
	if elements == nil {
		elements = []Form{}
	}
	return &form{head, Forms(elements)}, nil
}

// NewFormWithSymbol creates a form with a symbolic head
func NewFormWithSymbol(head string, context Context, elements ...Form) Form {
	if elements == nil {
		elements = []Form{}
	}
	return &form{NewSymbolicHead(head, context), Forms(elements)}
}

// NewFormWithKnownSymbol creates a form with a known symbolic head
func NewFormWithKnownSymbol(id uint64, head string, elements ...Form) Form {
	if elements == nil {
		elements = []Form{}
	}
	return &form{NewKnownSymbolicHead(id, head), Forms(elements)}
}

// NewFormWithFormHead creates a form with a form as the head
func NewFormWithFormHead(head Form, elements ...Form) Form {
	if elements == nil {
		elements = []Form{}
	}
	return &form{NewFormHead(head), Forms(elements)}
}

// Equal checks equality of forms
func Equal(a, b Form) bool {
	return Compare(a, b, func(a, b Form, _ Comparison) bool {
		return Equal(a, b)
	})
}

// A Comparison is used to implement a custom equality comparison between forms
type Comparison func(a, b Form, cmp Comparison) bool

// Compare checks equality of forms, using cmp to recurse
func Compare(a, b Form, cmp Comparison) bool {
	if a == nil || b == nil {
		return a == nil && b == nil
	}

	if !HeadsCompare(a.Head(), b.Head(), cmp) {
		return false
	}

	if a.Depth() != b.Depth() {
		return false
	}

	an, aIsNum := a.(NumericForm)
	bn, bIsNum := b.(NumericForm)
	ap, aIsPrim := a.(PrimitiveForm)
	bp, bIsPrim := b.(PrimitiveForm)
	at, aIsTensor := a.(TensorForm)
	bt, bIsTensor := b.(TensorForm)

	if aIsNum && !bIsNum || bIsNum && !aIsNum {
		return false
	}
	if aIsPrim && !bIsPrim || bIsPrim && !aIsPrim {
		return false
	}
	if aIsTensor && !bIsTensor || bIsTensor && !aIsTensor {
		return false
	}

	if aIsNum {
		if an.Value() != bn.Value() {
			var eq = false
			if !an.IsLarge() && !bn.IsLarge() {
				switch a.Head().Kind() {
				case IntegerHeadKind:
					eq = an.SmallIntegerValue() == bn.SmallIntegerValue()
				case RationalHeadKind:
					eq = an.SmallRationalValue() == bn.SmallRationalValue()
				case RealHeadKind:
					eq = an.SmallRealValue() == bn.SmallRealValue()
				}
			} else {
				switch a.Head().Kind() {
				case IntegerHeadKind:
					eq = an.LargeIntegerValue().Cmp(bn.LargeIntegerValue()) == 0
				case RationalHeadKind:
					eq = an.LargeRationalValue().Cmp(bn.LargeRationalValue()) == 0
				case RealHeadKind:
					eq = an.LargeRealValue().Cmp(bn.LargeRealValue()) == 0
				}
			}
			if !eq {
				return false
			}
		}
	} else if aIsPrim {
		if ap.Value() != bp.Value() {
			return false
		}
	} else if aIsTensor {
		if at.Dimensionality() != bt.Dimensionality() {
			return false
		}

		for i, x := range at.GetDimensions() {
			y, ok := bt.GetDimension(i)
			if !ok {
				return false
			}
			if x != y {
				return false
			}
		}
	}

	depth := a.Depth()
	if depth == 0 {
		return true
	}

	for i := uint32(0); i < depth; i++ {
		x, xok := a.GetElement(i)
		y, yok := b.GetElement(i)
		if !xok || !yok {
			if !xok && !yok {
				continue
			}
			return false
		}

		if !cmp(x, y, cmp) {
			return false
		}
	}

	return true
}

// HeadsEqual checks equality of form heads
func HeadsEqual(a, b Head) bool {
	return HeadsCompare(a, b, func(a, b Form, _ Comparison) bool {
		return Equal(a, b)
	})
}

// HeadsCompare checks equality of form heads, using cmp for heads that are forms
func HeadsCompare(a, b Head, cmp Comparison) bool {
	ak := a.Kind()
	bk := b.Kind()

	af, aIsForm := a.(FormHead)
	bf, bIsForm := b.(FormHead)

	as, aIsSym := a.(SymbolicHead)
	bs, bIsSym := b.(SymbolicHead)

	// if only one is a form head, they are not equal
	if aIsForm && !bIsForm || bIsForm && !aIsForm {
		return false
	}

	// if nither is symbolic, their kinds must match
	if !aIsSym && !bIsSym {
		// kind inequality is always head inequality (except for symbolic heads)
		if ak != bk {
			return false
		}

		// for non-symbolic, non-form heads, kind equality is head equality
		if !aIsForm {
			return true
		}
	}

	// if both are symbolic, ID inequality is head inequality (except if either ID is zero)
	if aIsSym && bIsSym {
		asid := as.ID()
		bsid := bs.ID()
		if asid > 0 && bsid > 0 && asid != bsid {
			return false
		}
	}

	if a.Name() != b.Name() {
		// name inequality is always head inequality
		return false
	} else if !aIsForm {
		// if nither is a form head, name equality is head equality
		return true
	}

	return cmp(af.Form(), bf.Form(), cmp)
}

type form struct {
	head Head
	Forms
}

func (f *form) String() string { return fmt.Sprintf("%v(%v)", f.head, f.Forms) }
func (f *form) Head() Head     { return f.head }

// Forms is a list of forms
type Forms []Form

func (l Forms) String() string {
	s := make([]string, len(l))
	for i, f := range l {
		s[i] = fmt.Sprint(f)
	}
	return strings.Join(s, ", ")
}

// Depth returns the number of forms in the list
func (l Forms) Depth() uint32 { return uint32(len(l)) }

// GetElements returns the forms as an array
func (l Forms) GetElements() []Form { return l }

// GetElement returns an indexed form
func (l Forms) GetElement(index uint32) (Form, bool) {
	if index >= uint32(len(l)) {
		return nil, false
	}
	return l[index], true
}

// HeadForm is the form used to extract the head of another form
type HeadForm struct{ Form Form }

func (f HeadForm) String() string { return fmt.Sprintf("Head(%v)", f.Form) }

// Head returns `HeadHead`
func (f HeadForm) Head() Head { return HeadHead }

// Depth returns `1`
func (f HeadForm) Depth() uint32 { return 1 }

// GetElements returns an array containing operand form
func (f HeadForm) GetElements() []Form { return []Form{f.Form} }

// GetElement returns the operand form for the zero index, or `nil, false` otherwise
func (f HeadForm) GetElement(index uint32) (Form, bool) {
	if index != 0 {
		return nil, false
	}
	return f.Form, true
}

// ContextForm is the form used to identify form contexts
type ContextForm uint32

func (f ContextForm) String() string { return fmt.Sprintf("Context(%d)", uint32(f)) }

// Head returns `ContextHead`
func (f ContextForm) Head() Head { return ContextHead }

// Depth returns `1`
func (f ContextForm) Depth() uint32 { return 1 }

// GetElements returns the an array containing the context number as a form
func (f ContextForm) GetElements() []Form { return []Form{SmallIntegerForm(f)} }

// GetElement returns the context number as a form for the zero index, or `nil, false` otherwise
func (f ContextForm) GetElement(index uint32) (Form, bool) {
	if index != 0 {
		return nil, false
	}
	return SmallIntegerForm(f), true
}
