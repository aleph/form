package form

import "fmt"

// HeadKind is a kind of head of forms
type HeadKind uint8

// MaxHeadKind is the maximum possible value of a head kind
//
// Head kind is varint encoded, so 127 is the maximum value that does not
// require an additional byte
const MaxHeadKind HeadKind = 0x7F

const (
	// SymbolicHeadKind is the kind of a head that is symbolic
	SymbolicHeadKind HeadKind = iota
	// NullHeadKind is the kind of the head of an empty form, i.e. ()
	NullHeadKind
	// SymbolHeadKind is the kind of the head of a symbol form
	SymbolHeadKind
	// TensorHeadKind is the kind of the head of a tensor form
	TensorHeadKind
	// FormHeadKind is the kind of a head that is itself a form
	FormHeadKind
	// SequenceHeadKind is the kind of a head of a form sequence
	SequenceHeadKind
)

const (
	// IntegerHeadKind is the kind of the head of an integer number form
	IntegerHeadKind HeadKind = iota + 8
	// RationalHeadKind is the kind of the head of a rational number form
	RationalHeadKind
	// RealHeadKind is the kind of the head of a real number form
	RealHeadKind
)

const (
	// StringHeadKind is the kind of the head of a string form
	StringHeadKind HeadKind = iota + 16
	// BooleanHeadKind is the kind of the head of a boolean form
	BooleanHeadKind
	// UUIDHeadKind is the kind of the head of a UUID form
	UUIDHeadKind
)

const (
	// IntegerTensorHeadKind is the kind of the head of a integer tensor form
	IntegerTensorHeadKind HeadKind = iota + 24
	// RationalTensorHeadKind is the kind of the head of a rational tensor form
	RationalTensorHeadKind
	// RealTensorHeadKind is the kind of the head of a real tensor form
	RealTensorHeadKind
)

const (
	// PartHeadKind is the kind of the head of a part operation
	PartHeadKind HeadKind = iota + 32
	// HeadHeadKind is the kind of the head of a head form
	HeadHeadKind
)

const (
	// ContextHeadKind is the kind of the head of a context form
	ContextHeadKind HeadKind = MaxHeadKind - iota
	// PacketHeadKind is the kind of the head of a packet form
	PacketHeadKind
)

func (k HeadKind) String() string {
	switch k {
	case SymbolicHeadKind:
		return "Symbolic"
	case NullHeadKind:
		return "Null"
	case SymbolHeadKind:
		return "Symbol"
	case TensorHeadKind:
		return "Tensor"
	case FormHeadKind:
		return "Form"
	case SequenceHeadKind:
		return "Sequence"
	case IntegerHeadKind:
		return "Integer"
	case RationalHeadKind:
		return "Rational"
	case RealHeadKind:
		return "Real"
	case StringHeadKind:
		return "String"
	case BooleanHeadKind:
		return "Boolean"
	case UUIDHeadKind:
		return "UUID"
	case IntegerTensorHeadKind:
		return "IntegerTensor"
	case RationalTensorHeadKind:
		return "RationalTensor"
	case RealTensorHeadKind:
		return "RealTensor"
	case PartHeadKind:
		return "Part"
	case HeadHeadKind:
		return "Head"
	case ContextHeadKind:
		return "Context"
	case PacketHeadKind:
		return "Packet"
	}

	return fmt.Sprintf("HeadKind:%d", k)
}

// KindHasWellDefinedForm returns whether the head kind has a well-defined form
func KindHasWellDefinedForm(kind HeadKind) bool {
	switch kind {
	case NullHeadKind, SymbolHeadKind, TensorHeadKind, SequenceHeadKind,
		IntegerHeadKind, RationalHeadKind, RealHeadKind, StringHeadKind, BooleanHeadKind, UUIDHeadKind,
		IntegerTensorHeadKind, RationalTensorHeadKind, RealTensorHeadKind,
		HeadHeadKind, ContextHeadKind:
		return true

	default:
		return false
	}
}

// KindIsPrimitive returns whether the head kind represents a primitive
func KindIsPrimitive(kind HeadKind) bool {
	switch kind {
	case NullHeadKind, SymbolHeadKind, SequenceHeadKind, IntegerHeadKind, RationalHeadKind, RealHeadKind, StringHeadKind, BooleanHeadKind, UUIDHeadKind:
		return true

	default:
		return false
	}
}
