package form

import (
	"fmt"
	"testing"

	"github.com/alecthomas/assert"
)

func must(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err)
	}
}

func assertString1(t *testing.T, s string, f Form) {
	assert.Equal(t, fmt.Sprint(f), s)
}

func assertString2(t *testing.T, s string) func(Form, error) {
	return func(f Form, err error) {
		must(t, err)
		assert.Equal(t, fmt.Sprint(f), s)
	}
}

func TestStrings(t *testing.T) {
	assertString2(t, "Test(1)")(New(NewUnknownSymbolicHead("Test"), SmallIntegerForm(1)))
	assertString1(t, "Tensor<2x2x2>(1, 2, 0.1, 0.2, 1/2, 3/2, '1', '2')", NewTensor([]uint32{2, 2, 2}, SmallIntegerForm(1), SmallIntegerForm(2), SmallRealForm(0.1), SmallRealForm(0.2), SmallRationalForm(rational(1, 2)), SmallRationalForm(rational(3, 2)), StringForm("1"), StringForm("2")))
	assertString1(t, "Tensor<2x2x2>(1, 2, 3, 4, 5, 6, 7, 8)", NewIntegerTensor([]uint32{2, 2, 2}, []int64{1, 2, 3, 4, 5, 6, 7, 8}))
}
