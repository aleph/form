package form

import (
	"fmt"
	"strings"

	"gitlab.com/aleph-project/form/value"
)

// NewIntegerList returns a list with the specified values
func NewIntegerList(values ...int64) Form {
	return IntegerListForm(values)
}

// IntegerListForm is a machine-precision integer array form
type IntegerListForm []int64

func (f IntegerListForm) String() string { return fmt.Sprintf("Tensor(%s)", f.ListString()) }

// ListString returns a string representation of the form's elements
func (f IntegerListForm) ListString() string {
	s := make([]string, len(f))
	for i, v := range f {
		s[i] = fmt.Sprint(v)
	}
	return strings.Join(s, ", ")
}

// Head returns `IntegerTensorHead`
func (f IntegerListForm) Head() Head { return IntegerTensorHead }

// Depth returns the number of elements of the tensor
func (f IntegerListForm) Depth() uint32 { return uint32(len(f)) }

// Dimensionality returns `1`
func (f IntegerListForm) Dimensionality() int { return 1 }

// GetDimensions returns a single-element array containing the depth
func (f IntegerListForm) GetDimensions() []uint32 { return []uint32{uint32(len(f))} }

// GetDimension returns the depth for index zero, or `0, false`
func (f IntegerListForm) GetDimension(index int) (uint32, bool) { return getDim1(f, index) }

// GetValue returns the indexed value for a single index, or `0, false`
func (f IntegerListForm) GetValue(indices ...uint32) (Form, bool) { return getValue1(f, indices) }

// GetElements returns an array of the values as forms
func (f IntegerListForm) GetElements() []Form {
	els := make([]Form, len(f))
	for i, value := range f {
		els[i] = SmallIntegerForm(value)
	}
	return els
}

// GetElement returns the indexed value as a form
func (f IntegerListForm) GetElement(index uint32) (Form, bool) {
	if index > uint32(len(f)) {
		return nil, false
	}
	return SmallIntegerForm(f[index]), true
}

// NewRationalList returns a list with the specified values
func NewRationalList(values ...value.Rational) Form {
	return RationalListForm(values)
}

// RationalListForm is a machine-precision rational array form
type RationalListForm []value.Rational

func (f RationalListForm) String() string { return fmt.Sprintf("Tensor(%s)", f.ListString()) }

// ListString returns a string representation of the form's elements
func (f RationalListForm) ListString() string {
	s := make([]string, len(f))
	for i, v := range f {
		s[i] = fmt.Sprint(v)
	}
	return strings.Join(s, ", ")
}

// Head returns `RationalTensorHead`
func (f RationalListForm) Head() Head { return RationalTensorHead }

// Depth returns the number of elements of the tensor
func (f RationalListForm) Depth() uint32 { return uint32(len(f)) }

// Dimensionality returns `1`
func (f RationalListForm) Dimensionality() int { return 1 }

// GetDimensions returns a single-element array containing the depth
func (f RationalListForm) GetDimensions() []uint32 { return []uint32{uint32(len(f))} }

// GetDimension returns the depth for index zero, or `0, false`
func (f RationalListForm) GetDimension(index int) (uint32, bool) { return getDim1(f, index) }

// GetValue returns the indexed value for a single index, or `0, false`
func (f RationalListForm) GetValue(indices ...uint32) (Form, bool) { return getValue1(f, indices) }

// GetElements returns an array of the values as forms
func (f RationalListForm) GetElements() []Form {
	els := make([]Form, len(f))
	for i, value := range f {
		els[i] = SmallRationalForm(value)
	}
	return els
}

// GetElement returns the indexed value as a form
func (f RationalListForm) GetElement(index uint32) (Form, bool) {
	if index > uint32(len(f)) {
		return nil, false
	}
	return SmallRationalForm(f[index]), true
}

// NewRealList returns a list with the specified values
func NewRealList(values ...float64) Form {
	return RealListForm(values)
}

// RealListForm is a machine-precision real array form
type RealListForm []float64

func (f RealListForm) String() string { return fmt.Sprintf("Tensor(%s)", f.ListString()) }

// ListString returns a string representation of the form's elements
func (f RealListForm) ListString() string {
	s := make([]string, len(f))
	for i, v := range f {
		s[i] = fmt.Sprint(v)
	}
	return strings.Join(s, ", ")
}

// Head returns `RealTensorHead`
func (f RealListForm) Head() Head { return RealTensorHead }

// Depth returns the number of elements of the tensor
func (f RealListForm) Depth() uint32 { return uint32(len(f)) }

// Dimensionality returns `1`
func (f RealListForm) Dimensionality() int { return 1 }

// GetDimensions returns a single-element array containing the depth
func (f RealListForm) GetDimensions() []uint32 { return []uint32{uint32(len(f))} }

// GetDimension returns the depth for index zero, or `0, false`
func (f RealListForm) GetDimension(index int) (uint32, bool) { return getDim1(f, index) }

// GetValue returns the indexed value for a single index, or `0, false`
func (f RealListForm) GetValue(indices ...uint32) (Form, bool) { return getValue1(f, indices) }

// GetElements returns an array of the values as forms
func (f RealListForm) GetElements() []Form {
	els := make([]Form, len(f))
	for i, value := range f {
		els[i] = SmallRealForm(value)
	}
	return els
}

// GetElement returns the indexed value as a form
func (f RealListForm) GetElement(index uint32) (Form, bool) {
	if index > uint32(len(f)) {
		return nil, false
	}
	return SmallRealForm(f[index]), true
}

// NewIntegerTensor returns a tensor with the specified values and dimensions
func NewIntegerTensor(dims []uint32, values []int64) Form {
	if len(dims) != 1 {
		return &IntegerTensorForm{IntegerListForm(values), dims}
	}

	d := dims[0]
	l := uint32(len(values))
	if d == l {
		return IntegerListForm(values)
	}
	if d < l {
		return IntegerListForm(values[:d])
	}

	fill := make([]int64, d-l)
	values = append(values, fill...)
	return IntegerListForm(values)
}

// IntegerTensorForm is a machine-precision integer tensor form
type IntegerTensorForm struct {
	IntegerListForm
	Dims Dimensions
}

func (f *IntegerTensorForm) String() string {
	return fmt.Sprintf("Tensor<%v>(%s)", f.Dims, f.IntegerListForm.ListString())
}

// Dimensionality returns the number of tensor dimensions
func (f *IntegerTensorForm) Dimensionality() int { return f.Dims.Dimensionality() }

// GetDimensions returns an array of the tensor dimensions
func (f *IntegerTensorForm) GetDimensions() []uint32 { return f.Dims.GetDimensions() }

// GetDimension returns the indexed dimension of the tensor
func (f *IntegerTensorForm) GetDimension(index int) (uint32, bool) { return f.Dims.GetDimension(index) }

// GetValue returns the indexed value of the tensor
func (f *IntegerTensorForm) GetValue(indices ...uint32) (Form, bool) {
	return f.Dims.GetValue(f, indices)
}

// NewRationalTensor returns a tensor with the specified values and dimensions
func NewRationalTensor(dims []uint32, values []value.Rational) Form {
	if len(dims) != 1 {
		return &RationalTensorForm{RationalListForm(values), dims}
	}

	d := dims[0]
	l := uint32(len(values))
	if d == l {
		return RationalListForm(values)
	}
	if d < l {
		return RationalListForm(values[:d])
	}

	fill := make([]value.Rational, d-l)
	values = append(values, fill...)
	return RationalListForm(values)
}

// RationalTensorForm is a machine-precision rational tensor form
type RationalTensorForm struct {
	RationalListForm
	Dims Dimensions
}

func (f *RationalTensorForm) String() string {
	return fmt.Sprintf("Tensor<%v>(%s)", f.Dims, f.RationalListForm.ListString())
}

// Dimensionality returns the number of tensor dimensions
func (f *RationalTensorForm) Dimensionality() int { return f.Dims.Dimensionality() }

// GetDimensions returns an array of the tensor dimensions
func (f *RationalTensorForm) GetDimensions() []uint32 { return f.Dims.GetDimensions() }

// GetDimension returns the indexed dimension of the tensor
func (f *RationalTensorForm) GetDimension(index int) (uint32, bool) { return f.Dims.GetDimension(index) }

// GetValue returns the indexed value of the tensor
func (f *RationalTensorForm) GetValue(indices ...uint32) (Form, bool) {
	return f.Dims.GetValue(f, indices)
}

// NewRealTensor returns a tensor with the specified values and dimensions
func NewRealTensor(dims []uint32, values []float64) Form {
	if len(dims) != 1 {
		return &RealTensorForm{RealListForm(values), dims}
	}

	d := dims[0]
	l := uint32(len(values))
	if d == l {
		return RealListForm(values)
	}
	if d < l {
		return RealListForm(values[:d])
	}

	fill := make([]float64, d-l)
	values = append(values, fill...)
	return RealListForm(values)
}

// RealTensorForm is a machine-precision real tensor form
type RealTensorForm struct {
	RealListForm
	Dims Dimensions
}

func (f *RealTensorForm) String() string {
	return fmt.Sprintf("Tensor<%v>(%s)", f.Dims, f.RealListForm.ListString())
}

// Dimensionality returns the number of tensor dimensions
func (f *RealTensorForm) Dimensionality() int { return f.Dims.Dimensionality() }

// GetDimensions returns an array of the tensor dimensions
func (f *RealTensorForm) GetDimensions() []uint32 { return f.Dims.GetDimensions() }

// GetDimension returns the indexed dimension of the tensor
func (f *RealTensorForm) GetDimension(index int) (uint32, bool) { return f.Dims.GetDimension(index) }

// GetValue returns the indexed value of the tensor
func (f *RealTensorForm) GetValue(indices ...uint32) (Form, bool) {
	return f.Dims.GetValue(f, indices)
}
