// nolint: golint
package form

import (
	"encoding/hex"
	"fmt"
	"math/big"
)

func String(v string) PrimitiveForm   { return StringForm(v) }
func Symbol(v string) PrimitiveForm   { return SymbolForm(v) }
func Sequence(v []Form) PrimitiveForm { return SequenceForm(v) }
func Boolean(v bool) PrimitiveForm    { return BooleanForm(v) }
func UUID(v [16]byte) PrimitiveForm   { return UUIDForm(v) }

func Integer(v int64) NumericForm { return SmallIntegerForm(v) }
func Real(v float64) NumericForm  { return SmallRealForm(v) }

func Rational(n int64, d int64) NumericForm {
	if n == 0 {
		return Integer(0)
	}
	if d == 1 {
		return Integer(n)
	}

	r := rational(n, d).Reduce()
	if r.IsInteger() {
		return Integer(r.Integer())
	}

	return SmallRationalForm(r)
}

func LargeInteger(v *big.Int) NumericForm {
	if v.IsInt64() {
		return Integer(v.Int64())
	}
	return (*LargeIntegerForm)(v)
}

func LargeReal(v *big.Float) NumericForm {
	if v.IsInt() {
		u, _ := v.Int(nil)
		return LargeInteger(u)
	}
	u, a := v.Float64()
	if a != big.Exact {
		return (*LargeRealForm)(v)
	}
	return Real(u)
}

func LargeRational(v *big.Rat) NumericForm {
	if v.IsInt() {
		return LargeInteger(v.Num())
	}
	if v.Num().IsInt64() && v.Denom().IsUint64() {
		return SmallRationalForm(rational(v.Num().Int64(), v.Denom().Int64()))
	}
	return (*LargeRationalForm)(v)
}

// compile-time check that primitives satisfy the interface
var (
	_ PrimitiveForm = new(NullForm)
	_ PrimitiveForm = new(SymbolForm)
	_ PrimitiveForm = new(SequenceForm)
	_ PrimitiveForm = new(StringForm)
	_ PrimitiveForm = new(BooleanForm)
	_ PrimitiveForm = new(UUIDForm)
)

// NullForm is the empty form primitive, i.e. ()
type NullForm struct{}

// Null is the empty form primitive, i.e. ()
var Null = NullForm{}

func (f NullForm) String() string                       { return "()" }
func (f NullForm) Head() Head                           { return NullHead }
func (f NullForm) Depth() uint32                        { return 0 }
func (f NullForm) GetElement(index uint32) (Form, bool) { return nil, false }
func (f NullForm) GetElements() []Form                  { return noElements }
func (f NullForm) Value() interface{}                   { return nil }

// SymbolForm is a symbolic form primitive
type SymbolForm string

func (f SymbolForm) String() string                       { return fmt.Sprintf("Symbol(%s)", f.Raw()) }
func (f SymbolForm) Raw() string                          { return string(f) }
func (f SymbolForm) Head() Head                           { return SymbolHead }
func (f SymbolForm) Depth() uint32                        { return 0 }
func (f SymbolForm) GetElement(index uint32) (Form, bool) { return nil, false }
func (f SymbolForm) GetElements() []Form                  { return noElements }
func (f SymbolForm) Value() interface{}                   { return f.Raw() }

// SequenceForm is a sequence form primitive
type SequenceForm []Form

func (f SequenceForm) String() string                       { return fmt.Sprintf("Sequence(%s)", f.Raw()) }
func (f SequenceForm) Raw() []Form                          { return []Form(f) }
func (f SequenceForm) Head() Head                           { return SequenceHead }
func (f SequenceForm) Depth() uint32                        { return 0 }
func (f SequenceForm) GetElement(index uint32) (Form, bool) { return nil, false }
func (f SequenceForm) GetElements() []Form                  { return noElements }
func (f SequenceForm) Value() interface{}                   { return f.Raw() }

// StringForm is a string form primitive
type StringForm string

func (f StringForm) String() string                       { return fmt.Sprintf("'%s'", f.Raw()) }
func (f StringForm) Raw() string                          { return string(f) }
func (f StringForm) Head() Head                           { return StringHead }
func (f StringForm) Depth() uint32                        { return 0 }
func (f StringForm) GetElement(index uint32) (Form, bool) { return nil, false }
func (f StringForm) GetElements() []Form                  { return noElements }
func (f StringForm) Value() interface{}                   { return f.Raw() }

// BooleanForm is a bool form primitive
type BooleanForm bool

func (f BooleanForm) String() string                       { return fmt.Sprintf("%t", f) }
func (f BooleanForm) Raw() bool                            { return bool(f) }
func (f BooleanForm) Head() Head                           { return BooleanHead }
func (f BooleanForm) Depth() uint32                        { return 0 }
func (f BooleanForm) GetElement(index uint32) (Form, bool) { return nil, false }
func (f BooleanForm) GetElements() []Form                  { return noElements }
func (f BooleanForm) Value() interface{}                   { return f.Raw() }

// BooleanForm is a UUID form primitive
type UUIDForm [16]byte

func (f UUIDForm) Raw() [16]byte                        { return [16]byte(f) }
func (f UUIDForm) Head() Head                           { return UUIDHead }
func (f UUIDForm) Depth() uint32                        { return 0 }
func (f UUIDForm) GetElement(index uint32) (Form, bool) { return nil, false }
func (f UUIDForm) GetElements() []Form                  { return noElements }
func (f UUIDForm) Value() interface{}                   { return f.Raw() }

func (f UUIDForm) String() string {
	var dst [42]byte
	copy(dst[:5], []byte("UUID("))
	hex.Encode(dst[5:13], f[:4])
	dst[13] = '-'
	hex.Encode(dst[14:18], f[4:6])
	dst[18] = '-'
	hex.Encode(dst[19:23], f[6:8])
	dst[23] = '-'
	hex.Encode(dst[24:28], f[8:10])
	dst[28] = '-'
	hex.Encode(dst[29:], f[10:])
	dst[41] = ')'
	return string(dst[:])
}
