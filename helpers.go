package form

import "gitlab.com/aleph-project/form/value"

var noElements = []Form{}

func getDim1(f TensorForm, index int) (uint32, bool) {
	if index == 0 {
		return f.Depth(), true
	}
	return 0, false
}

func getValue1(f TensorForm, indices []uint32) (Form, bool) {
	if len(indices) != 1 {
		return nil, false
	}
	return f.GetElement(indices[0])
}

func rational(nom int64, denom int64) value.Rational {
	return value.Rational{
		Numerator:   nom,
		Denominator: denom,
	}
}
