package form

import (
	"fmt"
	"strings"
)

// NewList returns a 1-dimensional tensor for the given values
func NewList(forms ...Form) Form {
	return ListForm(forms)
}

// ListForm is the form of a 1-dimensional tensor
type ListForm []Form

func (f ListForm) String() string { return fmt.Sprintf("Tensor(%v)", Forms(f)) }

// Head returns `TensorHead`
func (f ListForm) Head() Head { return TensorHead }

// Depth returns the number of elements
func (f ListForm) Depth() uint32 { return uint32(len(f)) }

// Dimensionality returns `1`
func (f ListForm) Dimensionality() int { return 1 }

// GetDimensions returns an array containing the list length
func (f ListForm) GetDimensions() []uint32 { return []uint32{uint32(len(f))} }

// GetElements returns an array of the tensor elements
func (f ListForm) GetElements() []Form { return f }

// GetDimension returns the list length for index zero, or `0, false`
func (f ListForm) GetDimension(index int) (uint32, bool) { return getDim1(f, index) }

// GetValue returns the index list value for a single index, or `nil, false`
func (f ListForm) GetValue(indices ...uint32) (Form, bool) { return getValue1(f, indices) }

// GetElement returns the index element of the list
func (f ListForm) GetElement(index uint32) (Form, bool) {
	if index > uint32(len(f)) {
		return nil, false
	}
	return f[index], true
}

// NewTensor returns a tensor with the specified values and dimensions
func NewTensor(dims []uint32, forms ...Form) Form {
	if len(dims) != 1 {
		return &GeneralTensorForm{Forms(forms), dims}
	}

	d := dims[0]
	l := uint32(len(forms))
	if d == l {
		return ListForm(forms)
	}
	if d < l {
		return ListForm(forms[:d])
	}

	fill := make([]Form, d-l)
	forms = append(forms, fill...)
	return ListForm(forms)
}

// GeneralTensorForm is the form of a generalized tensor
type GeneralTensorForm struct {
	Forms
	Dimensions
}

func (f *GeneralTensorForm) String() string {
	return fmt.Sprintf("Tensor<%v>(%v)", f.Dimensions, f.Forms)
}

// Head returns `TensorHead`
func (f *GeneralTensorForm) Head() Head { return TensorHead }

// GetValue returns the indexed value of the tensor
func (f *GeneralTensorForm) GetValue(indices ...uint32) (Form, bool) {
	return f.Dimensions.GetValue(f, indices)
}

// Dimensions are the dimensions of a tensor
type Dimensions []uint32

func (d Dimensions) String() string {
	s := make([]string, len(d))
	for i, d := range d {
		s[i] = fmt.Sprint(d)
	}
	return strings.Join(s, "x")
}

// Dimensionality returns the number of dimensions of the tensor
func (d Dimensions) Dimensionality() int { return len(d) }

// GetDimensions returns the dimensions of the tensor
func (d Dimensions) GetDimensions() []uint32 { return d }

// GetDimension returns the indexed dimension, or `0, false`
func (d Dimensions) GetDimension(index int) (uint32, bool) {
	if index < 0 || index >= len(d) {
		return 0, false
	}
	return d[index], true
}

// GetValue returns the indexed value of the tensor
func (d Dimensions) GetValue(f TensorForm, indices []uint32) (Form, bool) {
	if len(indices) != len(d) {
		return nil, false
	}

	var final uint32
	for i, index := range indices {
		final = final*d[i] + index
	}
	return f.GetElement(final)
}
