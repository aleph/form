package form

import "fmt"

var (
	// NullHead is the head of the empty form, i.e. ()
	NullHead Head = new(nullHead)
	// SymbolHead is the head of a symbolic form primitive
	SymbolHead Head = new(symbolHead)
	// TensorHead is the head of a tensor form
	TensorHead Head = new(tensorHead)
	// SequenceHead is the head of a sequence form
	SequenceHead Head = new(sequenceHead)
	// IntegerHead is the head of an integer numeric form primitive
	IntegerHead Head = new(integerHead)
	// RationalHead is the head of a rational numeric form primitive
	RationalHead Head = new(rationalHead)
	// RealHead is the head of a real numeric form primitive
	RealHead Head = new(realHead)
	// StringHead is the head of a string form primitive
	StringHead Head = new(stringHead)
	// BooleanHead is the head of a boolean form primitive
	BooleanHead Head = new(booleanHead)
	// UUIDHead is the head of a UUID form primitive
	UUIDHead Head = new(uuidHead)
	// IntegerTensorHead is the head of a integer tensor form
	IntegerTensorHead Head = new(integerTensorHead)
	// RationalTensorHead is the head of a rational tensor form
	RationalTensorHead Head = new(rationalTensorHead)
	// RealTensorHead is the head of a real tensor form
	RealTensorHead Head = new(realTensorHead)
	// PartHead is the head of a part form
	PartHead Head = new(partHead)
	// HeadHead is the head of a head form
	HeadHead Head = new(headHead)
	// ContextHead is the head of a context form
	ContextHead Head = new(contextHead)
	// PacketHead is the head of a packet form
	PacketHead Head = new(packetHead)
)

// NewHead creates a new head with the given name
//
// The context is used to check for known symbolic heads
func NewHead(name string, context Context) Head {
	switch name {
	case "Null":
		return NullHead
	case "Symbol":
		return SymbolHead
	case "Tensor":
		return TensorHead
	case "Sequence":
		return SequenceHead
	case "Integer":
		return IntegerHead
	case "Rational":
		return RationalHead
	case "Real":
		return RealHead
	case "String":
		return StringHead
	case "Boolean":
		return BooleanHead
	case "UUID":
		return UUIDHead
	case "Part":
		return PartHead
	case "Head":
		return HeadHead
	case "Context":
		return ContextHead
	case "Packet":
		return PacketHead
	}
	return NewSymbolicHead(name, context)
}

// NewSymbolicHead creates a new symbolic head with the given name
//
// The context is used to check for known symbolic heads
func NewSymbolicHead(name string, context Context) SymbolicHead {
	id, ok := context.GetKnownHeadID(name)
	if ok {
		return NewKnownSymbolicHead(id, name)
	}
	return NewUnknownSymbolicHead(name)
}

// NewKnownSymbolicHead creates a new SymbolicHead with a known symbol
func NewKnownSymbolicHead(id uint64, name string) SymbolicHead {
	return &symbolicHead{id, name}
}

// NewUnknownSymbolicHead creates a new head of kind UnknownHeadKind
func NewUnknownSymbolicHead(name string) SymbolicHead {
	return unknownSymbolicHead(name)
}

// NewFormHead creates a head from a form
func NewFormHead(form Form) FormHead {
	return &formHead{form}
}

// GetHeadForKind returns the head associated with a kind
//
// This works for head kinds that are defined, other than known and unknown
// heads.
func GetHeadForKind(kind HeadKind) (Head, bool) {
	switch kind {
	case NullHeadKind:
		return NullHead, true
	case SymbolHeadKind:
		return SymbolHead, true
	case TensorHeadKind:
		return TensorHead, true
	case SequenceHeadKind:
		return SequenceHead, true
	case IntegerHeadKind:
		return IntegerHead, true
	case RationalHeadKind:
		return RationalHead, true
	case RealHeadKind:
		return RealHead, true
	case StringHeadKind:
		return StringHead, true
	case BooleanHeadKind:
		return BooleanHead, true
	case UUIDHeadKind:
		return UUIDHead, true
	case IntegerTensorHeadKind:
		return IntegerTensorHead, true
	case RationalTensorHeadKind:
		return RationalTensorHead, true
	case RealTensorHeadKind:
		return RealTensorHead, true
	case PartHeadKind:
		return PartHead, true
	case HeadHeadKind:
		return HeadHead, true
	case ContextHeadKind:
		return ContextHead, true
	case PacketHeadKind:
		return PacketHead, true

	default:
		return nil, false
	}
}

type symbolicHead struct {
	id   uint64
	name string
}

func (h *symbolicHead) Kind() HeadKind { return SymbolicHeadKind }
func (h *symbolicHead) Name() string   { return h.name }
func (h *symbolicHead) ID() uint64     { return h.id }
func (h *symbolicHead) String() string { return h.Name() }

type unknownSymbolicHead string

func (h unknownSymbolicHead) Kind() HeadKind { return SymbolicHeadKind }
func (h unknownSymbolicHead) Name() string   { return string(h) }
func (h unknownSymbolicHead) ID() uint64     { return 0 }
func (h unknownSymbolicHead) String() string { return string(h) }

type formHead struct {
	form Form
}

func (h *formHead) Kind() HeadKind { return FormHeadKind }
func (h *formHead) Name() string   { return fmt.Sprint(h.form) }
func (h *formHead) String() string { return h.Name() }
func (h *formHead) Form() Form     { return h.form }

type nullHead struct{}

func (h *nullHead) Kind() HeadKind { return NullHeadKind }
func (h *nullHead) Name() string   { return "Null" }
func (h *nullHead) String() string { return h.Name() }

type symbolHead struct{}

func (h *symbolHead) Kind() HeadKind { return SymbolHeadKind }
func (h *symbolHead) Name() string   { return "Symbol" }
func (h *symbolHead) String() string { return h.Name() }

type tensorHead struct{}

func (h *tensorHead) Kind() HeadKind { return TensorHeadKind }
func (h *tensorHead) Name() string   { return "Tensor" }
func (h *tensorHead) String() string { return h.Name() }

type sequenceHead struct{}

func (h *sequenceHead) Kind() HeadKind { return SequenceHeadKind }
func (h *sequenceHead) Name() string   { return "Sequence" }
func (h *sequenceHead) String() string { return h.Name() }

type integerHead struct{}

func (h *integerHead) Kind() HeadKind { return IntegerHeadKind }
func (h *integerHead) Name() string   { return "Integer" }
func (h *integerHead) String() string { return h.Name() }

type rationalHead struct{}

func (h *rationalHead) Kind() HeadKind { return RationalHeadKind }
func (h *rationalHead) Name() string   { return "Rational" }
func (h *rationalHead) String() string { return h.Name() }

type realHead struct{}

func (h *realHead) Kind() HeadKind { return RealHeadKind }
func (h *realHead) Name() string   { return "Real" }
func (h *realHead) String() string { return h.Name() }

type stringHead struct{}

func (h *stringHead) Kind() HeadKind { return StringHeadKind }
func (h *stringHead) Name() string   { return "String" }
func (h *stringHead) String() string { return h.Name() }

type booleanHead struct{}

func (h *booleanHead) Kind() HeadKind { return BooleanHeadKind }
func (h *booleanHead) Name() string   { return "Boolean" }
func (h *booleanHead) String() string { return h.Name() }

type uuidHead struct{}

func (h *uuidHead) Kind() HeadKind { return UUIDHeadKind }
func (h *uuidHead) Name() string   { return "UUID" }
func (h *uuidHead) String() string { return h.Name() }

type integerTensorHead struct{}

func (h *integerTensorHead) Kind() HeadKind { return IntegerTensorHeadKind }
func (h *integerTensorHead) Name() string   { return "Tensor" }
func (h *integerTensorHead) String() string { return h.Name() }

type rationalTensorHead struct{}

func (h *rationalTensorHead) Kind() HeadKind { return RationalTensorHeadKind }
func (h *rationalTensorHead) Name() string   { return "Tensor" }
func (h *rationalTensorHead) String() string { return h.Name() }

type realTensorHead struct{}

func (h *realTensorHead) Kind() HeadKind { return RealTensorHeadKind }
func (h *realTensorHead) Name() string   { return "Tensor" }
func (h *realTensorHead) String() string { return h.Name() }

type partHead struct{}

func (h *partHead) Kind() HeadKind { return PartHeadKind }
func (h *partHead) Name() string   { return "Part" }
func (h *partHead) String() string { return h.Name() }

type headHead struct{}

func (h *headHead) Kind() HeadKind { return HeadHeadKind }
func (h *headHead) Name() string   { return "Head" }
func (h *headHead) String() string { return h.Name() }

type contextHead struct{}

func (h *contextHead) Kind() HeadKind { return ContextHeadKind }
func (h *contextHead) Name() string   { return "Context" }
func (h *contextHead) String() string { return h.Name() }

type packetHead struct{}

func (h *packetHead) Kind() HeadKind { return PacketHeadKind }
func (h *packetHead) Name() string   { return "Packet" }
func (h *packetHead) String() string { return h.Name() }
