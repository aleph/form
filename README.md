# Aleph: Forms

[![GoDoc](https://godoc.org/gitlab.com/aleph-project/form?status.svg)](https://godoc.org/gitlab.com/aleph-project/form)

The forms of Aleph are the core representations of information. Everything is a
form. Form is everything.

## Notes

Known head IDs start at 256, as the known head kind can also be used for heads
of well-defined forms, if the data do not conform to that well-defined form. For
example, `Head` can only be 'called' with a single argument; anything else is
invalid.

Primitive forms have zero depth and no elements, but have a value. Examples are
single numeric types, strings, and symbols.

Some head kinds have well-defined forms. Forms with heads of this kind MUST use
these well-defined forms and MUST NOT use the generic form. These well-defined
forms are critical to space and complexity optimizations. Components MUST be
able to safely assume that the concrete representation of a form of this kind is
the well-defined form. Constructing a form with a head of this kind that is not
the expected well-defined form will cause exceptional behavior.