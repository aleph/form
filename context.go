package form

// A Context stores IDs of known heads
type Context interface {
	// GetKnownHeadName returns the name of the known head with the specified ID
	GetKnownHeadName(id uint64) (name string, ok bool)
	// GetKnownHeadName returns the ID of the known head with the specified name
	GetKnownHeadID(name string) (id uint64, ok bool)
}

// NullContext is a context that does not know any heads
var NullContext nullContext

type nullContext struct{}

func (c nullContext) GetKnownHeadName(id uint64) (name string, ok bool) {
	return "", false
}

func (c nullContext) GetKnownHeadID(name string) (id uint64, ok bool) {
	return 0, false
}
