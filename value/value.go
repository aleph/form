package value

import (
	"fmt"
	"math/big"
)

// Rational is a rational value, with a numerator and a denominator
type Rational struct {
	Numerator   int64
	Denominator int64
}

// NewRational returns a normalized, reduced rational
func NewRational(num, denom int64) Rational {
	return Rational{num, denom}.Reduce()
}

// Integer returns the integer value of the rational
func (r Rational) Integer() int64 {
	return r.Numerator / r.Denominator
}

// Real returns the real value of the rational
func (r Rational) Real() float64 {
	return float64(r.Numerator) / float64(r.Denominator)
}

// Large converts the rational to a *big.Rat
func (r Rational) Large() *big.Rat {
	return big.NewRat(r.Numerator, r.Denominator)
}

// IsZero returns true if the numerator is zero
func (r Rational) IsZero() bool {
	return r.Numerator == 0
}

// IsInteger returns true if the denominator is one
func (r Rational) IsInteger() bool {
	r = r.Absolute()
	return r.Numerator%r.Denominator == 0
}

// Invert returns denominator / numerator
func (r Rational) Invert() Rational {
	return Rational{r.Denominator, r.Numerator}.Normalize()
}

func (r Rational) Negate() Rational {
	return Rational{-r.Numerator, r.Denominator}.Normalize()
}

// Normalize returns a normalized version of the rational
func (r Rational) Normalize() Rational {
	if r.Denominator == 0 {
		return r
	}

	if r.Denominator < 0 {
		r.Numerator = -r.Numerator
		r.Denominator = -r.Denominator
	}

	if r.Numerator%r.Denominator == 0 {
		r.Numerator /= r.Denominator
		r.Denominator = 1
	}

	return r
}

// Reduce attempts to reduce the rational to its simplest form
func (r Rational) Reduce() Rational {
	r = r.Normalize()
	s := r.Absolute()
	if s.Numerator%s.Denominator == 0 {
		return r
	}

	var a = s.Numerator
	var b = s.Denominator
	if a < 0 {
		a = -a
	}

	// remainder GCD algorithm
	for b != 0 {
		a, b = b, a%b
	}

	if a == 1 {
		return r
	}

	r = Rational{r.Numerator / a, r.Denominator / a}
	return r
}

// Absolute returns the absolute value of the rational
func (r Rational) Absolute() Rational {
	if r.Numerator < 0 {
		r.Numerator = -r.Numerator
	}
	if r.Denominator < 0 {
		r.Denominator = -r.Denominator
	}
	return r.Normalize()
}

// Add returns r + s, reduced
func (r Rational) Add(s Rational) Rational {
	// TODO check for overflow
	return Rational{
		r.Numerator*int64(s.Denominator) + s.Numerator*r.Denominator,
		r.Denominator * s.Denominator,
	}.Reduce()
}

// Multiply returns r * s, reduced
func (r Rational) Multiply(s Rational) Rational {
	// TODO check for overflow
	return Rational{
		r.Numerator * s.Numerator,
		r.Denominator * s.Denominator,
	}.Reduce()
}

// Subtract returns r - s, reduced
func (r Rational) Subtract(s Rational) Rational {
	// TODO check for overflow
	return Rational{
		r.Numerator*int64(s.Denominator) - s.Numerator*r.Denominator,
		r.Denominator * s.Denominator,
	}.Reduce()
}

// Divide returns r / s, reduced
func (r Rational) Divide(s Rational) Rational {
	// TODO check for overflow
	var sign int64 = 1
	if s.Numerator < 0 {
		sign = -1
	}
	return Rational{
		sign * r.Numerator * s.Denominator,
		r.Denominator * s.Numerator,
	}.Reduce()
}

func (r Rational) String() string { return fmt.Sprintf("%d/%d", r.Numerator, r.Denominator) }
