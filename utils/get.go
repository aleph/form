package utils

import "gitlab.com/aleph-project/form"

// GetNumber returns the specified element of a form as a numeric form
func GetNumber(f form.Form, index uint32) (form.NumericForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return nil, false
	}
	h, ok := g.(form.NumericForm)
	if !ok {
		return nil, false
	}
	return h, true
}

// GetNumbers returns the elements of a form as an array of numeric forms
func GetNumbers(f form.Form) (values []form.NumericForm, large bool, ok bool) {
	var n = f.Depth()
	var v form.NumericForm

	values = make([]form.NumericForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = GetNumber(f, i)
		if !ok {
			return
		}
		values[i] = v
		if v.IsLarge() {
			large = true
		}
	}

	return
}

// GetString returns the specified element of a form as a string form
func GetString(f form.Form, index uint32) (form.StringForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return "", false
	}
	h, ok := g.(form.StringForm)
	if !ok {
		return "", false
	}
	return h, true
}

// GetStrings returns the elements of a form as an array of string forms
func GetStrings(f form.Form) (values []form.StringForm, ok bool) {
	var n = f.Depth()
	var v form.StringForm

	values = make([]form.StringForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = GetString(f, i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}

// GetSymbol returns the specified element of a form as a symbol form
func GetSymbol(f form.Form, index uint32) (form.SymbolForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return "", false
	}
	h, ok := g.(form.SymbolForm)
	if !ok {
		return "", false
	}
	return h, true
}

// GetSymbols returns the elements of a form as an array of symbol forms
func GetSymbols(f form.Form) (values []form.SymbolForm, ok bool) {
	var n = f.Depth()
	var v form.SymbolForm

	values = make([]form.SymbolForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = GetSymbol(f, i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}

// GetUUID returns the specified element of a form as a UUID form
func GetUUID(f form.Form, index uint32) (form.UUIDForm, bool) {
	g, ok := f.GetElement(index)
	if !ok {
		return form.UUIDForm{}, false
	}
	h, ok := g.(form.UUIDForm)
	if !ok {
		return form.UUIDForm{}, false
	}
	return h, true
}

// GetUUIDs returns the elements of a form as an array of UUID forms
func GetUUIDs(f form.Form) (values []form.UUIDForm, ok bool) {
	var n = f.Depth()
	var v form.UUIDForm

	values = make([]form.UUIDForm, n)
	for i := uint32(0); i < n; i++ {
		v, ok = GetUUID(f, i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}

// GetValues copies the elements of a form
//
// Fails the form does not have N elements
func GetValues(f form.Form, N uint32) (values []form.Form, ok bool) {
	var n = f.Depth()
	var v form.Form

	if N > 0 && n != N {
		return
	}

	values = make([]form.Form, n)
	for i := uint32(0); i < n; i++ {
		v, ok = f.GetElement(i)
		if !ok {
			return
		}
		values[i] = v
	}

	return
}
