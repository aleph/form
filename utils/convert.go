package utils

import "gitlab.com/aleph-project/form"

// GetHeadAsForm returns the head of a form as a form
func GetHeadAsForm(x form.Form) form.Form {
	return HeadAsForm(x.Head())
}

// HeadAsForm returns a head as a form
func HeadAsForm(x form.Head) form.Form {
	switch y := x.(type) {
	case form.FormHead:
		return y.Form()
	default:
		return form.Symbol(x.Name())
	}
}

// GetHeadAsSymbol returns the head of a form as a symbol
func GetHeadAsSymbol(x form.Form) (form.SymbolForm, bool) {
	if x == nil {
		return "", false
	}

	return HeadAsSymbol(x.Head())
}

// HeadAsSymbol returns the name of a head as a symbol
func HeadAsSymbol(x form.Head) (form.SymbolForm, bool) {
	if x == nil {
		return "", false
	}

	sym, ok := HeadAsForm(x).(form.SymbolForm)
	if ok {
		return sym, true
	}

	return "", false
}

// GetSymbolValue returns the symbolic value of a form
func GetSymbolValue(f form.Form) (string, bool) {
	if f == nil {
		return "", false
	}

	if s, ok := f.(form.SymbolForm); ok {
		return s.Raw(), true
	}

	p, ok := f.(form.PrimitiveForm)
	if !ok {
		return "", false
	}

	v, ok := p.Value().(string)
	if !ok {
		return "", false
	}
	return v, true
}
